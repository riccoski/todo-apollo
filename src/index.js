import React from "react";
import ReactDOM from "react-dom";
import ApolloClient from "apollo-boost";
import gql from "graphql-tag";
import { ApolloProvider } from "react-apollo";

import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

const GET_TODOS = gql`
  {
    todos @client {
      id
      title
      completed
    }
  }
`;

const client = new ApolloClient({
  clientState: {
    defaults: {
      todos: []
    },
    resolvers: {
      Query: {
        Todo: {
          ClientSideField(parent) {
            // here I can add a new field to everything todo CLIENTSIDE

            return "todo:" + parent.title;
          }
        }
      },
      Mutation: {
        addTodo(_, todoData, { cache }) {
          todoData.__typename = "Todo";
          const data = cache.readQuery({ query: GET_TODOS });

          cache.writeData({
            data: {
              todos: [...data.todos, todoData]
            }
          });

          return todoData;
        }
      }
    }
  }
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
