import React from "react";
import "./App.css";
import gql from "graphql-tag";
import { Query } from "react-apollo";

const GET_TODOS = gql`
  {
    todos @client {
      id
      title
      completed
    }
  }
`;

const Todos = () => (
  <Query query={GET_TODOS}>
    {({ loading, error, data }) => {
      if (loading) return "Loading...";
      if (error) return `Error! ${error.message}`;

      return (
        <ul>
          {data.todos.map(todo => (
            <Todo key={todo.id} todo={todo} />
          ))}
        </ul>
      );
    }}
  </Query>
);

class TodoModel {
  constructor(data) {
    Object.assign(this, data);
  }
}

export default Todos;
// *** RE RENDERS ALL 'TODO' Components
// const Todo = ({ todo }) => (
//   <li key={todo.id}>
//     {todo.title}
//     {console.log("testttt")}
//   </li>
// );

// *** RE RENDERS ALL 'TODO' Components
class Todo extends React.PureComponent {
  render() {
    const { todo } = this.props;
    return (
      <li key={todo.id}>
        {todo.title}
        {console.log("testttt")}
      </li>
    );
  }
}
