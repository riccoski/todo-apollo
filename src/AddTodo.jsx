import React, { PureComponent } from "react";
import uuid from "uuid/v4";
import "./App.css";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

const ADD_TODO = gql`
  mutation addTodo($title: String!, $id: String!, $completed: false) {
    addTodo(title: $title, id: $id, completed: $completed) @client {
      id
      title
      completed
    }
  }
`;

class AddTodo extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      title: ""
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const { value } = e.target;

    this.setState({
      title: value
    });
  }

  render() {
    const { title } = this.state;
    return (
      <Mutation mutation={ADD_TODO}>
        {(addTodo, { data }) => (
          <div>
            <form
              onSubmit={e => {
                e.preventDefault();
                addTodo({
                  variables: {
                    id: uuid(),
                    title: title,
                    completed: false
                  }
                });
                this.setState({
                  title: ""
                });
              }}
            >
              <input value={title} onChange={this.handleChange} />
              <button type="submit">Add Todo</button>
            </form>
          </div>
        )}
      </Mutation>
    );
  }
}

export default AddTodo;
