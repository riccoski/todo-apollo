import React from "react";
import "./App.css";
import AddTodo from "./AddTodo";
import Todos from "./Todos";

const App = () => (
  <div className="App">
    <header className="App-header">
      <AddTodo />
      <Todos />
    </header>
  </div>
);

export default App;
